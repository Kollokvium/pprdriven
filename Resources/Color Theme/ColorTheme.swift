import UIKit
import Foundation

public class ColorTheme {
    private struct GeneralColors {
        static let black: UIColor = .black
        static let white: UIColor = .white
        static let orange: UIColor = .orange
        static let gray: UIColor = .gray
    }
    
    public struct AppThemeColors {
        static let mainThemeBackgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        static let lightPinkColor = #colorLiteral(red: 1, green: 0.5409764051, blue: 0.8473142982, alpha: 1)
        static let nicePinkColor = #colorLiteral(red: 1, green: 0.262745098, blue: 0.6235294118, alpha: 1)
        static let niceRedColor = #colorLiteral(red: 1, green: 0.1882352941, blue: 0.1803921569, alpha: 1)
    }
    
    // MARK: Markets Screen + Context
    public var generalTextColor: UIColor = GeneralColors.black
    public var marketsNameTextColor: UIColor = AppThemeColors.niceRedColor
}
