import Foundation

class Localizer {
    
    public static func localizedString(string: String, comment: String = "") -> String {
        return NSLocalizedString(string, comment: comment)
    }
}
