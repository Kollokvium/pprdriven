import Foundation
import UIKit

extension UIViewController {
    class func instantiate<T: UIViewController>(storyboardName: String? = nil, identifier: String? = nil) -> T {
        let storyboard = UIStoryboard(name: storyboardName ?? "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: identifier ?? String(describing: T.self)) as! T
    }
}
