import UIKit

class MarketsViewController: UIViewController {
    
    @IBOutlet private weak var tableBoardTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableBoardTableView.rowHeight = UITableView.automaticDimension
        tableBoardTableView.estimatedRowHeight = 60
        tableBoardTableView.reloadData()
        navigationItem.title = "Markets"
    }
    
    struct Props {
        var markets: [MarketModelCellTableViewCell.Props]
    }
    
    var props = Props(markets: []) {
        didSet {
            guard isViewLoaded else { return }
            DispatchQueue.main.async {
                self.tableBoardTableView.reloadData()
            }
        }
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        if parent == self.navigationController?.parent {
            print("Back button touch")
        }
    }
}

// MARK: - DataSource & Delegate
extension MarketsViewController: UITableViewDelegate {
    
}

extension MarketsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return props.markets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MarkDataCell") as? MarketModelCellTableViewCell else { fatalError() }
        cell.props = props.markets[indexPath.row]
        return cell
    }
}
