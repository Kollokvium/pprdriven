import UIKit

class MarketModelCellTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var marketCellView: UIView!
    @IBOutlet private weak var minPriceLabel: UILabel!
    @IBOutlet private weak var maxPriceLabel: UILabel!
    @IBOutlet private weak var currentMarketLabel: UILabel!
    
    let formatter = NumberFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 8
        currentMarketLabel.textColor = UIColor(named: "NiceRedColor")
        minPriceLabel.textColor = ColorTheme.AppThemeColors.niceRedColor
        maxPriceLabel.textColor = ColorTheme.AppThemeColors.niceRedColor
    }
    
    struct Props {
        let minPriceText: Double
        let maxPriceText: Double
        let marketText: String
        
        static let empty = Props(minPriceText: 0.0, maxPriceText: 0.0, marketText: "")
    }
    
    var props = Props.empty {
        didSet {
            minPriceLabel.text = formatter.string(from: NSNumber(value: props.minPriceText))
            maxPriceLabel.text = formatter.string(from: NSNumber(value: props.maxPriceText))
            currentMarketLabel.text = props.marketText
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        props = Props.empty
    }
    
}
