import UIKit
import Foundation

class DispatchViewController: UIViewController {
    
    var myWords = ["Some", "Chick", "Turn", "Around"]
    var math = [1, 4, 5, 10]
    var compactArray = [["🐵", "🐵", "🐵"], ["🦊", "🦊"], ["🦁", "🦁"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Dispatch"
        
        map()
        filter()
        reduce()
        compact()
    }
    
    // MARK: - .map
    // Loops over a collection and applies the same operation to each element in the collection.
    func map() {
        let sentence = myWords.map { $0 + " 1" }
        
        let sentence2 = myWords.map({(value:String) -> String in
            return value + " 2"
        })
        
        let sentence3 = myWords.map { (value:String) in
            return value + " 3"
        }
        
        let sentence4 = myWords.map { value in value + " 4" }
        
        print("""
            \(sentence),
            \(sentence2),
            \(sentence3),
            \(sentence4)
            """)
    }
    
    // MARK: - .filter
    // Loops over a collection and returns an array that contains elements that meet a condition.
    
    func filter() {
        let fileteredSentence = myWords.filter { $0 == "Chick" || $0 == "Around" }
        print(fileteredSentence)
    }
    
    // MARK: - .reduce
    // Loops over a collection and Combines all items in a collection to create a single value. aka Glue
    
    func reduce() {
        let calculation = math.reduce(0, +)
        print(calculation)
        
        let glue = myWords.reduce("", { $0 + $1 })
        print(glue)
    }
    
    // MARK: - .flatMap
    // Arrays within an array that we would like to combine into a single array.
    
    func compact() {
        let zooArray = compactArray.flatMap { $0 }
        print(compactArray)
        print("Zoo: \(zooArray)")
        // .flatMap removes nil from the collection
    }
}
