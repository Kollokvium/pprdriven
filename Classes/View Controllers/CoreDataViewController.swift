import UIKit
import CoreData

class CoreDataViewController: UIViewController {
    
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let passwordGenerator = PasswordGenerator()
    let ageGenerator = AgeGenerator()
    let usernameGenerator = UserNameGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Core Data"
    }
    
    func newUserGenerating() {
        guard let context = appDelegate?.persistentContainer.viewContext else { return }
        guard let entity = NSEntityDescription.entity(forEntityName: "Users", in: context) else { return }
        
        let newUser = NSManagedObject(entity: entity, insertInto: context)
        newUser.setValue(String(usernameGenerator.generateUserName()), forKey: "username")
        newUser.setValue(String(passwordGenerator.generatePassword()), forKey: "password")
        newUser.setValue(String(ageGenerator.generateUserAge()), forKey: "age")
        
        print(newUser)
    }
    
    func newUserFetch() {
        let context = appDelegate?.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context?.fetch(request)
            for data in result as! [NSManagedObject] {
                userNameLabel.text = data.value(forKey: "username") as? String
                passwordLabel.text = data.value(forKey: "password") as? String
                ageLabel.text = data.value(forKey: "age") as? String
                print(data.value(forKey: "username") as! String)
            }
        } catch {
            print("Fetch User error: \(error.localizedDescription)")
        }
    }
    
    @IBAction func setupNewUser(_ sender: UIButton) {
        newUserGenerating()
        appDelegate?.saveContext()
    }
    
    @IBAction func userDataFetch(_ sender: UIButton) {
        newUserFetch()
    }
}
