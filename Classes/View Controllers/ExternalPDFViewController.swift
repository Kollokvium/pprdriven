import UIKit
import WebKit
import PDFKit

class ExternalPDFViewController: UIViewController, WKUIDelegate {
    
    var initialLoadedWebView: WKWebView!
    let webConfiguration = WKWebViewConfiguration()
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        initialLoadedWebView = WKWebView(frame: .zero, configuration: webConfiguration)
        initialLoadedWebView.uiDelegate = self
        view = initialLoadedWebView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "External PDF"
        
        loadContentFromPDFLink()
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(ExternalPDFViewController.saveDocument))
        navigationItem.rightBarButtonItem = saveButton
    }
    
    func loadContentFromPDFLink() {
        let myURL = URL(string: "https://www.ets.org/Media/Tests/TOEFL/pdf/SampleQuestions.pdf")
        let myRequest = URLRequest(url: myURL!)
        initialLoadedWebView.load(myRequest)
    }
    
    @objc func saveDocument() {
        DownloaderService.loadFile()
    }
}
