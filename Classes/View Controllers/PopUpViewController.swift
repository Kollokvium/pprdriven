import UIKit

class PopUpViewController: UIViewController {
    
    @IBOutlet weak var popUpTitleLabel: UILabel!
    @IBOutlet weak var popUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpPopUpLayout()
    }
    
    func setUpPopUpLayout() {
        popUpButton.layer.cornerRadius = 14
    }
    
    @IBAction func popUpButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
