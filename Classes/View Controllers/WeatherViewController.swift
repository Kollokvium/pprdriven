import UIKit

class WeatherViewController: UIViewController {
    
    @IBOutlet private weak var cityNameLabel: UILabel!
    
    @IBOutlet private weak var longDataLabel: UILabel!
    @IBOutlet private weak var latDataLabel: UILabel!
    
    @IBOutlet private weak var mainDataLabel: UILabel!
    @IBOutlet private weak var mainDescriptionDataLabel: UILabel!
    
    @IBOutlet private weak var temperatureDataLabel: UILabel!
    @IBOutlet private weak var pressureDataLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Weather"
    }
    
    struct Props {
        let cityName: String
        let longtitude: Double
        let latitude: Double
        let windSpeed: Double
        let windDirection: Double
        let temperature: Double
        let pressure: Double
    }
    
    var props = Props(cityName: "Loading City Name...",
                      longtitude: 0.0,
                      latitude: 0.0,
                      windSpeed: 0.0,
                      windDirection: 0.0,
                      temperature: 0.0,
                      pressure: 0) {
        didSet {
            DispatchQueue.main.async {
                self.view.setNeedsLayout()
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        cityNameLabel.text = props.cityName
        longDataLabel.text = String(describing: props.longtitude)
        latDataLabel.text = String(describing: props.latitude)
        mainDataLabel.text = String(describing: props.windSpeed)
        mainDescriptionDataLabel.text = String(describing: props.windDirection)
        temperatureDataLabel.text = String(describing: props.temperature)
        pressureDataLabel.text = String(describing: props.pressure)
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        if parent == self.navigationController?.parent {
            print("Back button touch")
        }
    }
}
