import UIKit
import Foundation

public class MainViewController: UIViewController {
    
    @IBOutlet private weak var viewControllersCollectionLabel: UILabel!
    let services = Services()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        title = "App Navigation"
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            if let _ : Bool? = self.services.userDefaultsService.getObject(forKey: UserDefaultService.Constants.oneTimePopUp) {
                
            } else {
                self.props.oneTimePopUpCall()
                self.services.userDefaultsService.setObject(object: true, forKey: UserDefaultService.Constants.oneTimePopUp)
            }
        })
    }
    
    struct Props {
        let collectionViewControllers: String
        
        let openWeatherViewController: () -> ()
        let openUserInterfaceViewController: () -> ()
        let openMarketsViewController: () -> ()
        let openDispatchViewController: () -> ()
        let openCoreDataViewController: () -> ()
        let openExternalPDFViewController: () -> ()
        let oneTimePopUpCall: () -> ()
    }
    
    var props = Props(
        collectionViewControllers: "",
        openWeatherViewController: {},
        openUserInterfaceViewController: {},
        openMarketsViewController: {},
        openDispatchViewController: {},
        openCoreDataViewController: {},
        openExternalPDFViewController: {},
        oneTimePopUpCall: {}) {
        didSet {
            view.setNeedsLayout()
            DispatchQueue.main.async {
                self.viewControllersCollectionLabel.text = self.props.collectionViewControllers
            }
        }
    }
    
    @IBAction func weatherScreenTransiton(_ sender: UIButton) {
        props.openWeatherViewController()
    }
    
    @IBAction func userInterfaceScreenTransiton(_ sender: UIButton) {
        props.openUserInterfaceViewController()
    }
    
    @IBAction func marketsScreenTransiton(_ sender: UIButton) {
        props.openMarketsViewController()
    }
    
    @IBAction func dispatchScreenTransiton(_ sender: UIButton) {
        props.openDispatchViewController()
    }
    
    @IBAction func coreDataScreenTransiton(_ sender: UIButton) {
        props.openCoreDataViewController()
    }
    
    @IBAction func externalPDFScreenTransition(_ sender: UIButton) {
        props.openExternalPDFViewController()
    }
    
    func oneTimePopUpCall() {
        props.oneTimePopUpCall()
    }
}
