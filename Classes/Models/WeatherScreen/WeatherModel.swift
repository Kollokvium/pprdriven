import Foundation

struct WeatherModel: Codable {
    let coord: Coord
    var weather: [Weather]
    let base: String?
    let main: Main
    let visibility: Int?
    let wind: Wind
    let clouds: Clouds
    let dt: Int?
    let sys: Sys
    let id: Int?
    let name: String?
    let cod: Int?
}

struct Coord: Codable {
    let lon: Double
    let lat: Double
}

struct Clouds: Codable {
    let all: Int
}

struct Main: Codable {
    let temp: Double
    let pressure: Double
    let humidity: Int
    let temp_min: Double
    let temp_max: Double
}

struct Sys: Codable {
    let type: Int?
    let id: Int?
    let message: Double?
    let country: String?
    let sunrise: Int?
    let sunset: Int?
}

struct Weather: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct Wind: Codable {
    let speed: Double
    let deg: Double
}
