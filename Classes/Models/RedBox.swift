import UIKit

struct RedBox: Codable {
    let possitionX : CGFloat?
    let possitionY : CGFloat?
    let widthSize : Double?
    let heightSize : Double?
}
