import Foundation

struct ResultModel: Codable {
    let marketName: String?
    let high: Double?
    let low: Double?
    let volume: Double?
    let last: Double?
    let baseVolume: Double?
    let timeStamp: String?
    let bid: Double?
    let ask: Double?
    let openBuyOrders: Int?
    let openSellOrders: Int?
    let prevDay: Double?
    let created: String?
    
    enum CodingKeys: String, CodingKey {
        case marketName = "MarketName"
        case high = "High"
        case low = "Low"
        case volume = "Volume"
        case last = "Last"
        case baseVolume = "BaseVolume"
        case timeStamp = "TimeStamp"
        case bid = "Bid"
        case ask = "Ask"
        case openBuyOrders = "OpenBuyOrders"
        case openSellOrders = "OpenSellOrders"
        case prevDay = "PrevDay"
        case created = "Created"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        marketName = try values.decodeIfPresent(String.self, forKey: .marketName)
        high = try values.decodeIfPresent(Double.self, forKey: .high)
        low = try values.decodeIfPresent(Double.self, forKey: .low)
        volume = try values.decodeIfPresent(Double.self, forKey: .volume)
        last = try values.decodeIfPresent(Double.self, forKey: .last)
        baseVolume = try values.decodeIfPresent(Double.self, forKey: .baseVolume)
        timeStamp = try values.decodeIfPresent(String.self, forKey: .timeStamp)
        bid = try values.decodeIfPresent(Double.self, forKey: .bid)
        ask = try values.decodeIfPresent(Double.self, forKey: .ask)
        openBuyOrders = try values.decodeIfPresent(Int.self, forKey: .openBuyOrders)
        openSellOrders = try values.decodeIfPresent(Int.self, forKey: .openSellOrders)
        prevDay = try values.decodeIfPresent(Double.self, forKey: .prevDay)
        created = try values.decodeIfPresent(String.self, forKey: .created)
    }
}
