import Foundation

public struct Services {
    let marketsApiService: MarketsApiService
    let weatherApiService: WeatherApiService
    let downloaderService: DownloaderService
    let layoutSaverService: LayoutSaverService
    let userDefaultsService: UserDefaultService
    
    public init() {
        self.marketsApiService = MarketsApiService()
        self.weatherApiService = WeatherApiService()
        self.downloaderService = DownloaderService()
        self.layoutSaverService = LayoutSaverService()
        self.userDefaultsService = UserDefaultService()
    }
}
