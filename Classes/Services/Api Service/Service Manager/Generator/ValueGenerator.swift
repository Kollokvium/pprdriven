import Foundation

struct PasswordGenerator {
    let passwordCharacters = Array("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
    let passwordLenght = 10
    
    func generatePassword() -> String {
        let randomPassword = String((0..<passwordLenght).map { _ in passwordCharacters[Int(arc4random_uniform(UInt32(passwordCharacters.count)))]})
        return randomPassword
    }
}

struct UserNameGenerator {
    func generateUserName() -> String {
        return "Anton"
    }
}

struct AgeGenerator {
    let maxAge = 116
    let minAge = 18
    
    func generateUserAge() -> Int {
        return Int(arc4random_uniform(UInt32(maxAge-minAge)) + UInt32(minAge))
    }
}
