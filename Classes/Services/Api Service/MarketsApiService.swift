import Foundation

public enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}

class MarketsApiService {
    enum Constants {
        static let baseURL = "https://bittrex.com/api/v1.1/public/"
        static let summary = "getmarketsummaries"
    }
    
    enum ApiServiceError: Error {
        case empty
        case decoder(error: Error)
    }
    
    func getAllMarkets(completion: @escaping (_ result: Result<MarketModel, ApiServiceError>) -> Void) {
        guard let bittrexUrl = URL(string: Constants.baseURL+Constants.summary) else { return }
        URLSession.shared.dataTask(with: bittrexUrl) { data, response, error in
            guard let data = data else {
                return completion(.failure(.empty))
            }
            do {
                let decoder = JSONDecoder()
                let bittrexData = try decoder.decode(MarketModel.self, from: data)
                completion(.success(bittrexData))
            } catch {
                completion(.failure(.decoder(error: error)))
            }
            }.resume()
    }
}
