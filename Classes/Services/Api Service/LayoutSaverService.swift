import Foundation

class LayoutSaverService {
    let box = RedBox(possitionX: 0.0,
                     possitionY: 0.0,
                     widthSize: 25.0,
                     heightSize: 25.0)
    
    func redBoxEncoding() {
        var json: Any?
        
        //Encode RedBox Struct as Data
        let encodedData = try? JSONEncoder().encode(box)
        
        if let data = encodedData {
            json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let json = json {
                print("Red Box JSON: \(json)")
            }
        }
    }
    
    func sendRedBoxToServer() {
        // Send parameters
        let parameters = ["heightSize": box.heightSize ?? "",
                          "possitionX": box.possitionY ?? "",
                          "possitionY": box.possitionY ?? "",
                          "widthSize": box.widthSize ?? ""] as Dictionary<String, Any>
        
        let url = URL(string: "www.google.com")
        let session = URLSession.shared
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
}
