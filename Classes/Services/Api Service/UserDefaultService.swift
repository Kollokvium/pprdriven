import Foundation

class UserDefaultService {
    
    enum Constants {
        static let oneTimePopUp = "isShowOneTimePopUp"
    }
    
    func setObject<T>(object: T, forKey: String) {
        UserDefaults.standard.set(object, forKey: forKey)
    }
    
    func getObject<T>(forKey: String) -> T? {
        return UserDefaults.standard.object(forKey: forKey) as? T
    }
    
    func removeObject(forKey: String) {
        UserDefaults.standard.removeObject(forKey: forKey)
    }
}
