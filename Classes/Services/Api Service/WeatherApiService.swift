import Foundation

class WeatherApiService {
    enum ApiServiceError: Error {
        case empty
        case decoder(error: Error)
    }
    
    enum Constants {
        static let baseURL = "http://api.openweathermap.org/data/"
        static let apiVersion = "2.5/"
        static let weatherDataType = "weather?q="
        static let cityRequest = "LasVegas"
        static let apiKey = "&appid=a2e284ed02784218c7f3367c4e8ec167"
    }
    
    func getCurrentWeather(completion: @escaping (_ result: Result<WeatherModel, ApiServiceError>) -> Void) {
        guard let weatherURL = URL(string:
            Constants.baseURL +
                Constants.apiVersion +
                Constants.weatherDataType +
                Constants.cityRequest +
                Constants.apiKey) else { return }
        
        URLSession.shared.dataTask(with: weatherURL) { data, response, error in
            guard let data = data else {
                return completion(.failure(.empty))
            }
            do {
                let decoder = JSONDecoder()
                let weatherData = try decoder.decode(WeatherModel.self, from: data)
                completion(.success(weatherData))
            } catch let error {
                completion(.failure(.decoder(error: error)))
            }
            }.resume()
    }
}
