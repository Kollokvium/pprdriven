import Foundation
import UIKit

class WeatherCoordinator: RootViewCoordinator {
    var services: Services
    var childCoordinators: [Coordinator] = []
    
    var rootViewController: UIViewController {
        return self.navigationController
    }
    
    private var navigationController: UINavigationController
    
    // MARK: - Init
    init(with services: Services, navigation: UINavigationController) {
        self.services = services
        self.navigationController = navigation
        showSimpleViewController()
    }
    
    func showSimpleViewController() {
        let viewController: WeatherViewController = WeatherViewController.instantiate(
            storyboardName: "WeatherScreen",
            identifier: "WeatherViewController")
        
        DispatchQueue.main.async {
            self.navigationController.pushViewController(viewController, animated: true)
        }
        
        services.weatherApiService.getCurrentWeather { weatherData in
            switch weatherData {
            case .failure(let error):
                print(error)
            case .success(let weather):
                viewController.props = .init(cityName: weather.name ?? "Some Name",
                                             longtitude: weather.coord.lon,
                                             latitude: weather.coord.lat,
                                             windSpeed: weather.wind.speed,
                                             windDirection: weather.wind.deg,
                                             temperature: weather.main.temp,
                                             pressure: weather.main.pressure)
            }
        }
    }
}
