import Foundation
import UIKit

class MarketsCoordinator: RootViewCoordinator {
    var services: Services
    var childCoordinators: [Coordinator] = []
    
    var rootViewController: UIViewController {
        return self.navigationController
    }
    
    private var navigationController: UINavigationController
    
    // MARK: - Init
    init(with services: Services, navigation: UINavigationController) {
        self.services = services
        self.navigationController = navigation
        showTableViewController()
    }
    
    // MARK: - Functions
    func showTableViewController() {
        let viewController: MarketsViewController = MarketsViewController.instantiate(
            storyboardName: "MarketsScreen",
            identifier: "MarketsViewController")
        
        DispatchQueue.main.async {
            self.navigationController.pushViewController(viewController, animated: true)
        }
        
        services.marketsApiService.getAllMarkets { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let markers):
                viewController.props = .init(
                    markets: markers.result.map {
                        .init(
                            minPriceText: $0.low ?? 0,
                            maxPriceText: $0.high ?? 0,
                            marketText: $0.marketName ?? "No Market"
                        )}
                )}
        }
    }
}
