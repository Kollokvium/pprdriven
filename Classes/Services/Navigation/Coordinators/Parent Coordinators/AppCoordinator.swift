import Foundation
import UIKit

// The AppCoordinator is first coordinator
class AppCoordinator: RootViewCoordinator {
    
    // MARK: - Properties
    let services: Services
    var childCoordinators: [Coordinator] = []
    
    var rootViewController: UIViewController {
        return self.navigationController
    }
    
    let window: UIWindow
    
    private lazy var navigationController: UINavigationController = {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = false
        return navigationController
    }()
    
    // MARK: - Init
    public init(window: UIWindow, services: Services) {
        self.services = services
        self.window = window
        
        self.window.rootViewController = self.rootViewController
        self.window.makeKeyAndVisible()
    }
    
    // MARK: - Functions
    /// Starts the coordinator
    public func start() {
        self.showMainViewController()
    }
    
    /// Creates a new mainViewController and places it into the navigation controller
    private func showMainViewController() {
        
        let mainViewController: MainViewController = MainViewController.instantiate(storyboardName: "Main",
                                                                                    identifier: "MainScreenViewController")
        self.navigationController.viewControllers = [mainViewController]
        navigationController.navigationBar.prefersLargeTitles = true
        
        mainViewController.props = MainViewController.Props.init(
            collectionViewControllers: Localizer.localizedString(string: "Stack View of View Controllers"),
            openWeatherViewController: {
                self.openWeatherViewController()
        }, openUserInterfaceViewController: {
            self.openUserInterfaceViewController()
        }, openMarketsViewController: {
            self.openMarketsViewController()
        }, openDispatchViewController: {
            self.openDispatchViewController()
        }, openCoreDataViewController: {
            self.openCoreDataViewController()
        }, openExternalPDFViewController: {
            self.openExternalPDFViewController()
        }, oneTimePopUpCall: {
            self.openPopUp()
        })
    }
}

// MARK: - MainViewControllerDelegate
extension AppCoordinator {
    
    func openWeatherViewController() {
        // Weather Coordinator
        let weatherCoordinator = WeatherCoordinator(with: services, navigation: navigationController)
        
        if childCoordinators.count == 1 {
            childCoordinators.removeAll()
            childCoordinators.append(weatherCoordinator)
            print("One Coordinator at place. Remove old one, create and append new one. New Count check:", childCoordinators.count)
        } else {
            childCoordinators.append(weatherCoordinator)
            print("Zero Coordinators. Append one. Coordinator Count:", childCoordinators.count)
        }
    }
    
    func openMarketsViewController() {
        // Markets Coordinator
        let marketsCoordinator = MarketsCoordinator(with: services, navigation: navigationController)
        if childCoordinators.count == 1 {
            childCoordinators.removeAll()
            childCoordinators.append(marketsCoordinator)
            print("One Coordinator at place. Remove old one, create and append new one. New Count check:", childCoordinators.count)
        } else {
            childCoordinators.append(marketsCoordinator)
            print("Zero Coordinators. Append one. Coordinator Count:", childCoordinators.count)
        }
    }
    
    func openUserInterfaceViewController() {
        let viewController: UserInterfaceViewController = UserInterfaceViewController.instantiate(storyboardName: "UserInterfaceScreen",
                                                                                                  identifier: "UserInterfaceViewController")
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func openDispatchViewController() {
        let viewController: DispatchViewController = DispatchViewController.instantiate(storyboardName: "DispatchScreen",
                                                                                        identifier: "DispatchViewController")
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func openCoreDataViewController() {
        let viewController: CoreDataViewController = CoreDataViewController.instantiate(storyboardName: "CoreDataScreen",
                                                                                        identifier: "CoreDataViewController")
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func openExternalPDFViewController() {
        let viewController: ExternalPDFViewController = ExternalPDFViewController.instantiate(storyboardName: "ExternalPDFScreen",
                                                                                              identifier: "ExternalPDFViewController")
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func openPopUp() {
        let popUpViewController: PopUpViewController = PopUpViewController.instantiate(storyboardName: "PopUp",
                                                                                       identifier: "PopUpViewController")
        popUpViewController.modalPresentationStyle = .overCurrentContext
        navigationController.present(popUpViewController, animated: true, completion: nil)
    }
}
